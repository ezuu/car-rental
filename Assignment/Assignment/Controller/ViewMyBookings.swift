//
//  ViewMyBookings.swift
//  Assignment
//
//  Created by Vikneswaran on 28/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
import Firebase

class ViewMyBookings: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let cellId = "cellId"
    var bookingsArray = [Bookings]()
    var ref : DatabaseReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference(fromURL: "https://assignment-2b2b4.firebaseio.com/")
        navigationItem.title = "My Bookings"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        
        setupCollectionView()
        retrieveData()
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        return collectionView
    }()
    
    func setupCollectionView() {
        view.addSubview(collectionView)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.frame = view.frame
    }
    
    func retrieveData() {
        guard let userId = Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("Bookings").child(userId).observe(.value, with: { (snapshot) in
            
            if let values = snapshot.value as? NSDictionary {
                let bookings = values.allKeys
                self.bookingsArray.removeAll()
                
                for booking in bookings {
                    let bookingData = values[booking] as! [String : String]
                    
                    for (_, _) in bookingData {
                        let bookingDate = bookingData["bookingDate"]
                        let pickedCar = bookingData["pickedCar"]
                        let deliveryAddress = bookingData["deliveryAddress"]
                        let pickupAddress = bookingData["pickupAddress"]
                        let pickupDate = bookingData["pickupDate"]
                        let deliveryDate = bookingData["deliveryDate"]
                        let rentPrice = bookingData["price"]
                        let bookingId = bookingData["bookId"]
                        let bookedCar = bookingData["bookedCar"]

                        
                        let book = Bookings(bookingDate: bookingDate!, pickedCar: pickedCar!, deliveryAddress: deliveryAddress!, pickupAddress: pickupAddress!, pickupDate: pickupDate!, deliveryDate: deliveryDate!, price: rentPrice!, bookingId: bookingId!, bookedCar: bookedCar!)
                        self.bookingsArray.append(book)
                        break
                    }
                    
                    self.collectionView.reloadData()
                }
            }
            
        }, withCancel: nil)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bookingsArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CollectionViewCell
        
        cell.bookingDateLabel.text = "Booking Date: \(bookingsArray[indexPath.row].bookingDate)"
        cell.carLabel.text = "Booked Car: \(bookingsArray[indexPath.row].pickedCar)"
        cell.pickupDateLabel.text = "Pickup Date: \(bookingsArray[indexPath.row].pickupDate)"
        cell.deliveryDateLabel.text = "Delivery Date: \(bookingsArray[indexPath.row].deliveryDate)"
        cell.pickupAddressLabel.text = "Pickup Address: \(bookingsArray[indexPath.row].pickupAddress)"
        cell.deliveryAddressLabel.text = "Delivery Address: \(bookingsArray[indexPath.row].deliveryAddress)"
        cell.priceLabel.text = "Price: \(bookingsArray[indexPath.row].price)"
        
        let returnButton: UIButton = {
            let button = UIButton(type: .system)
            button.backgroundColor = UIColor(r: 52, g: 73, b: 94)
            button.setTitle("Return", for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitleColor(UIColor.white, for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            button.layer.cornerRadius = 15
            button.tag = indexPath.row
            button.addTarget(self, action: #selector(handleReturn), for: .touchUpInside)
            return button
        }()
        
        cell.addSubview(returnButton)
        
        returnButton.topAnchor.constraint(equalTo: cell.priceLabel.bottomAnchor, constant: 20).isActive = true
        returnButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        returnButton.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 12).isActive = true
        returnButton.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: -12).isActive = true
        
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: view.frame.width, height: 550)
    }
    
    @objc func handleReturn(button: UIButton) {
        let indexPath = button.tag
        let ind = IndexPath(item: indexPath, section: 0)
        let bookingData = bookingsArray[indexPath]
        
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        
        self.updateCar(bookedCar: bookingData.bookedCar)
        
        Database.database().reference().child("Bookings").child(userId).child(bookingData.bookingId).removeValue { (error, ref) in
            guard let errorMessage = error?.localizedDescription else {return}
            
            if error != nil {
                Alert.showPopup(title: "Error", message: errorMessage, viewController: self)
            }
            
            self.bookingsArray.remove(at: indexPath)
            self.collectionView.deleteItems(at: [ind])
            
        }
    }
    
    func updateCar(bookedCar : String) {
        ref?.child("Cars").observe(.value, with: { (snapshot) in
            if let values = snapshot.value as? NSDictionary {
                let cars = values.allKeys
                for car in cars {
                    let carData = values[car] as! [String : String]
                    for (_, _) in carData {
                        let carUniq = carData["carId"]
                        if carUniq == bookedCar {
                            let dataRef = self.ref?.child("Cars").child(bookedCar)
                            let value = ["isBooked" : "false"]
                            dataRef?.updateChildValues(value, withCompletionBlock: { (error, ref) in
                                if error != nil {
                                    Alert.showPopup(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                                }
                            })
                        }
                        break
                    }
                }

            }
        })
    }

}
