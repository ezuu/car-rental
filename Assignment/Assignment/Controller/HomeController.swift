//
//  HomeController.swift
//  Assignment
//
//  Created by Vikneswaran on 26/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
import Firebase
class HomeController: UIViewController {
    
    var ref : DatabaseReference?
    var isShowing: Bool = false
    var carModel = ""
    var carMake = ""
    var isBooked = ""
    var numberOfSeats = ""
    var yearMake = ""
    var carId = ""
    var carsArray = [Cars]()
    var carName = [String]()
    var selectedCarModel : String?
    var selectedCarId : String?
    var pickedCarId = ""
    var price = 0

    let carPicker = UIPickerView()
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        ref = Database.database().reference(fromURL: "https://assignment-2b2b4.firebaseio.com/")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(handleShowMenu))
        navigationItem.title = "Book Car"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor(r: 42, g: 77, b: 105)
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        }
        
        setupView()
        createPicker()
        toolBar()
        createDatePicker()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchForAvailableCar()
    }

    let menuView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 38, g: 50, b: 56)
        view.addCustomMenuShadow()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let logoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Logout", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = UIColor(r: 69, g: 90, b: 100)
        let color = UIColor(r: 255, g: 82, b: 82)
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleLogout), for: .touchUpInside)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let viewProfile: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("View Profile", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = UIColor(r: 69, g: 90, b: 100)
        let color = UIColor(r: 255, g: 82, b: 82)
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleViewProfile), for: .touchUpInside)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let viewMyBookings: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("My Bookings", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = UIColor(r: 69, g: 90, b: 100)
        let color = UIColor(r: 255, g: 82, b: 82)
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleViewMyBookings), for: .touchUpInside)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let homeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Home", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.backgroundColor = UIColor(r: 69, g: 90, b: 100)
        let color = UIColor(r: 255, g: 82, b: 82)
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleHome), for: .touchUpInside)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let pickCarTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Pick Car"
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let pickupDate: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Pickup Date"
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let deliveryDate: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Delivery Date"
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let pickupAddress: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Pickup Address"
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let deliveryAddress: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Delivery Address"
        textField.backgroundColor = UIColor.white
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        return view
    }()
    
    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 52, g: 73, b: 94)
        button.setTitle("Next", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        return button
    }()
    
    var rightAnchor : NSLayoutConstraint?
    var containerLeadingAnchor : NSLayoutConstraint?
    
    func toolBar() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(HomeController.dismissKeyboard))
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        pickCarTextField.inputAccessoryView = toolbar
        pickupDate.inputAccessoryView = toolbar
        deliveryDate.inputAccessoryView = toolbar
    }
    
    func createPicker() {
        carPicker.delegate = self
        carPicker.dataSource = self
        carPicker.backgroundColor = UIColor.white

        pickCarTextField.inputView = carPicker
  
    }
    
    func createDatePicker() {
        datePicker.datePickerMode = .dateAndTime
        datePicker.minimumDate = Date()
        pickupDate.inputView = datePicker
        deliveryDate.inputView = datePicker
    }
    
    @objc func dismissKeyboard() {
        if pickupDate.isFirstResponder == true {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
            pickupDate.text = dateFormatter.string(from: datePicker.date)
        } else if deliveryDate.isFirstResponder == true {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
            deliveryDate.text = dateFormatter.string(from: datePicker.date)
        }
        view.endEditing(true)
    }
    
    func setupView() {
        view.addSubview(menuView)
        view.addSubview(container)
        menuView.addSubview(logoutButton)
        menuView.addSubview(homeButton)
        menuView.addSubview(viewMyBookings)
        menuView.addSubview(viewProfile)
        
        container.addSubview(pickCarTextField)
        container.addSubview(pickupDate)
        container.addSubview(deliveryDate)
        container.addSubview(pickupAddress)
        container.addSubview(deliveryAddress)
        container.addSubview(nextButton)
        
        rightAnchor = menuView.rightAnchor.constraint(equalTo: view.leftAnchor, constant: -10)
        rightAnchor?.isActive = true
        menuView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        menuView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        menuView.widthAnchor.constraint(equalToConstant: view.frame.width/2.5).isActive = true
        
        //Mark:- Button
        homeButton.topAnchor.constraint(equalTo: menuView.topAnchor, constant: 250).isActive = true
        homeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        homeButton.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: 5).isActive = true
        homeButton.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: -5).isActive = true
        
        viewProfile.topAnchor.constraint(equalTo: homeButton.bottomAnchor, constant: 20).isActive = true
        viewProfile.heightAnchor.constraint(equalToConstant: 40).isActive = true
        viewProfile.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: 5).isActive = true
        viewProfile.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: -5).isActive = true
        
        viewMyBookings.topAnchor.constraint(equalTo: viewProfile.bottomAnchor, constant: 20).isActive = true
        viewMyBookings.heightAnchor.constraint(equalToConstant: 40).isActive = true
        viewMyBookings.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: 5).isActive = true
        viewMyBookings.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: -5).isActive = true
        
        logoutButton.topAnchor.constraint(equalTo: viewMyBookings.bottomAnchor, constant: 20).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        logoutButton.leadingAnchor.constraint(equalTo: menuView.leadingAnchor, constant: 5).isActive = true
        logoutButton.trailingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: -5).isActive = true
        
        //Mark:- Container
        container.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        container.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        container.leadingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: 20).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        //Mark:- Text Fields
        pickCarTextField.topAnchor.constraint(equalTo: container.topAnchor, constant: 70).isActive = true
        pickCarTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickCarTextField.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        pickCarTextField.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true

        pickupDate.topAnchor.constraint(equalTo: pickCarTextField.bottomAnchor, constant: 15).isActive = true
        pickupDate.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupDate.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        pickupDate.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        deliveryDate.topAnchor.constraint(equalTo: pickupDate.bottomAnchor, constant: 15).isActive = true
        deliveryDate.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryDate.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        deliveryDate.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        pickupAddress.topAnchor.constraint(equalTo: deliveryDate.bottomAnchor, constant: 15).isActive = true
        pickupAddress.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupAddress.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        pickupAddress.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        deliveryAddress.topAnchor.constraint(equalTo: pickupAddress.bottomAnchor, constant: 15).isActive = true
        deliveryAddress.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryAddress.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        deliveryAddress.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        //Mark:- Next Button
        nextButton.topAnchor.constraint(equalTo: deliveryAddress.bottomAnchor, constant: 25).isActive = true
        nextButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        nextButton.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        nextButton.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
    }
    
    @objc func handleShowMenu() {
        rightAnchor?.isActive = false
        let constraintUpdate: NSLayoutConstraint = isShowing ? menuView.rightAnchor.constraint(equalTo: view.leftAnchor, constant: -10) : menuView.rightAnchor.constraint(equalTo: view.leftAnchor, constant: menuView.frame.width)
        rightAnchor = constraintUpdate
        rightAnchor?.isActive = true

        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
       
        let updateConstraint: NSLayoutConstraint = isShowing ? container.leadingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: 20) : container.leadingAnchor.constraint(equalTo: menuView.trailingAnchor, constant: 150)
        containerLeadingAnchor = updateConstraint
        containerLeadingAnchor?.isActive = true
        
        isShowing = !isShowing
    }
    
    @objc func handleLogout() {
        do {
            try Auth.auth().signOut()
        } catch let error {
            print(error)
        }
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
    }
    
    func searchForAvailableCar() {
        ref?.child("Cars").observe(.value, with: { (snapshot) in

            self.carName.removeAll()
            self.carsArray.removeAll()
            
            if let values = snapshot.value as? NSDictionary {
                let cars = values.allKeys
            
                for car in cars {
                    let carData = values[car] as! [String : String]
                    
                    for (_, _) in carData {
                        let checkBooked = carData["isBooked"]
                        if checkBooked == "false" {
                            self.carModel = carData["carModel"]!
                            self.carMake = carData["carMake"]!
                            self.numberOfSeats = carData["numberOfSeats"]!
                            self.yearMake = carData["yearMake"]!
                            self.isBooked = carData["isBooked"]!
                            self.carId = carData["carId"]!
                            
                            let carr = Cars(carMake: self.carMake, carModel: self.carModel, isBooked: self.isBooked, numberOfSeats: self.numberOfSeats, yearMake: self.yearMake, cardId: self.carId)
                            self.carsArray.append(carr)
                            self.carName.append(self.carModel)
                        }
                        break
                    }
                }
            }
        })
    }
    
    @objc func handleNext() {

        print("cheeck: \(carsArray)")
        guard let car = pickCarTextField.text else {return}
        guard let pickDate = pickupDate.text else {return}
        guard let delivDate = deliveryDate.text else {return}
        guard let pickAddress = pickupAddress.text else {return}
        guard let deliverAddress = deliveryAddress.text else {return}
        if car != "" && pickDate != "" && delivDate != "" && pickAddress != "" && deliverAddress != "" {
            let reviewBooking = ReviewBooking()
            reviewBooking.carArray = carsArray
            reviewBooking.pickedCar = car
            reviewBooking.selectedPickupDate = pickDate
            reviewBooking.selectedDeliveryDate = delivDate
            reviewBooking.pickedAddress = pickAddress
            reviewBooking.deliveredAdress = deliverAddress
            reviewBooking.pickedCaridd = pickedCarId
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm"
            guard let d1 = dateFormatterGet.date(from: pickDate) else {return}
            guard let d2 = dateFormatterGet.date(from: delivDate) else {return}
            
            guard let diffInDays = Calendar.current.dateComponents([.day], from: d1, to: d2).day else {return}
            
            let bookingPrice = calculatePrice(date1: pickDate, date2: delivDate, diffInDays: diffInDays)
            reviewBooking.rentPrice = "RM \(bookingPrice)"
           
            navigationController?.pushViewController(reviewBooking, animated: true)
        } else {
            Alert.showPopup(title: "Error", message: "Key in All fields", viewController: self)
        }
    }
    
    func calculatePrice(date1: String, date2: String, diffInDays: Int) -> Int {
        
        if diffInDays <= 2 && diffInDays > 0{
            price = diffInDays * 80
            return price
        } else if diffInDays <= 5 && diffInDays > 2{
            price = diffInDays * 70
            return price
        } else if diffInDays > 5 {
            price = diffInDays * 60
            return price
        } else {
            return price
        }
        
    }
    
    @objc func handleHome() {
        let homeController = HomeController()
        self.navigationController?.pushViewController(homeController, animated: true)
    }
    
    @objc func handleViewProfile() {
        let viewProfile = ViewProfile()
        self.navigationController?.pushViewController(viewProfile, animated: true)
    }
    
    @objc func handleViewMyBookings() {
        let viewMyBookings = ViewMyBookings()
        self.navigationController?.pushViewController(viewMyBookings, animated: true)
    }
}

extension UIView {
    func addCustomMenuShadow() {
        self.layer.shadowOffset = CGSize(width: 5, height: self.frame.height)
        self.layer.shadowOpacity = 0.1
    }
}

extension HomeController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if !carName.isEmpty {
            return carName.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         if !carName.isEmpty {
            return carName[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == carPicker {
            if !carName.isEmpty {
                selectedCarModel = carsArray[row].carModel
                selectedCarId = carsArray[row].cardId
                pickCarTextField.text = selectedCarModel
                pickedCarId = selectedCarId!
            }
        }
    }
    
    
}











