//
//  Review.swift
//  Assignment
//
//  Created by Vikneswaran on 27/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
import Firebase
class ReviewBooking : UIViewController {
    
    var ref : DatabaseReference?
    var carArray = [Cars]()
    var pickedCar = ""
    var selectedPickupDate = ""
    var selectedDeliveryDate = ""
    var pickedAddress = ""
    var deliveredAdress = ""
    var pickedCaridd = ""
    var rentPrice = ""
    var uuid = NSUUID().uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference(fromURL: "https://assignment-2b2b4.firebaseio.com/")
        navigationItem.title = "Review Bookings"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        setupView()
    }
    
    let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        return view
    }()

    let carLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let pickupDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let deliveryDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let pickupAddressLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let deliveryAddressLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bookButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 52, g: 73, b: 94)
        button.setTitle("Book", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(handleBook), for: .touchUpInside)
        return button
    }()
    
    func setupView() {
        view.addSubview(container)
        container.addSubview(carLabel)
        container.addSubview(pickupDateLabel)
        container.addSubview(deliveryDateLabel)
        container.addSubview(pickupAddressLabel)
        container.addSubview(deliveryAddressLabel)
        container.addSubview(priceLabel)
        container.addSubview(bookButton)
        
        //Mark:- Container
        container.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        container.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        container.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        //Mark:- Label
        carLabel.topAnchor.constraint(equalTo: container.topAnchor, constant: 70).isActive = true
        carLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        carLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        carLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        pickupDateLabel.topAnchor.constraint(equalTo: carLabel.bottomAnchor, constant: 20).isActive = true
        pickupDateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupDateLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        pickupDateLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        deliveryDateLabel.topAnchor.constraint(equalTo: pickupDateLabel.bottomAnchor, constant: 20).isActive = true
        deliveryDateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryDateLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        deliveryDateLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        pickupAddressLabel.topAnchor.constraint(equalTo: deliveryDateLabel.bottomAnchor, constant: 20).isActive = true
        pickupAddressLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupAddressLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        pickupAddressLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        deliveryAddressLabel.topAnchor.constraint(equalTo: pickupAddressLabel.bottomAnchor, constant: 20).isActive = true
        deliveryAddressLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryAddressLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        deliveryAddressLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        priceLabel.topAnchor.constraint(equalTo: deliveryAddressLabel.bottomAnchor, constant: 20).isActive = true
        priceLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        priceLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        priceLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        //Mark:- Book Button
        bookButton.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 20).isActive = true
        bookButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        bookButton.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 12).isActive = true
        bookButton.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        carLabel.text = pickedCar
        pickupDateLabel.text = selectedPickupDate
        deliveryDateLabel.text = selectedDeliveryDate
        pickupAddressLabel.text = pickedAddress
        deliveryAddressLabel.text = deliveredAdress
        priceLabel.text = rentPrice
    }
    
    @objc func handleBook() {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let today = dateFormatter.string(from: date)
        
        guard let userId = Auth.auth().currentUser?.uid else {return}
        let bookId = uuid
        let book = self.ref?.child("Bookings").child(userId).child(bookId)
        let values = ["pickedCar" : pickedCar as String,
                      "pickupDate" : selectedPickupDate as String,
                      "deliveryDate" : selectedDeliveryDate as String,
                      "pickupAddress" : pickedAddress as String,
                      "deliveryAddress" : deliveredAdress as String,
                      "bookingDate" : today as String,
                      "bookedCar" : pickedCaridd as String,
                      "price" : rentPrice as String,
                      "bookId" : bookId as String]
        book?.updateChildValues(values, withCompletionBlock: { (error, databasereference) in
            if error != nil {
                Alert.showPopup(title: "Error", message: (error?.localizedDescription)!, viewController: self)
            }
            self.updateBookedCar(pickedCar: self.pickedCaridd)
            let homeController = HomeController()
            self.navigationController?.pushViewController(homeController, animated: true)
        })
        
    }
    
    func updateBookedCar(pickedCar: String) {
        
        ref?.child("Cars").observe(.value, with: { (snapshot) in
            if let values = snapshot.value as? NSDictionary {
                let cars = values.allKeys
                
                for car in cars {
                    let carData = values[car] as! [String : String]
                    for (_, _) in carData {
                        let carn = carData["carId"]
                        if carn == self.pickedCaridd {
                            let dataRef = self.ref?.child("Cars").child(self.pickedCaridd)
                            let value = ["isBooked" : "true"]
                            dataRef?.updateChildValues(value, withCompletionBlock: { (error, ref) in
                                if error != nil {
                                    Alert.showPopup(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                                }
                            })
                        }
                        break
                    }
                }
            }
            
            
        }, withCancel: nil)
    }
    

}
