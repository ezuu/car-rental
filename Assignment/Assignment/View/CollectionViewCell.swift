//
//  CollectionViewCell.swift
//  Assignment
//
//  Created by Vikneswaran on 28/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    var btnReturnAction : (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let carLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let pickupDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let deliveryDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let pickupAddressLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let deliveryAddressLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bookingDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    func setupView() {
        addSubview(carLabel)
        addSubview(pickupDateLabel)
        addSubview(deliveryDateLabel)
        addSubview(pickupAddressLabel)
        addSubview(deliveryAddressLabel)
        addSubview(bookingDateLabel)
        addSubview(priceLabel)
        
        bookingDateLabel.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        bookingDateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        bookingDateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        bookingDateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        carLabel.topAnchor.constraint(equalTo: bookingDateLabel.bottomAnchor, constant: 20).isActive = true
        carLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        carLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        carLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        pickupDateLabel.topAnchor.constraint(equalTo: carLabel.bottomAnchor, constant: 20).isActive = true
        pickupDateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupDateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        pickupDateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        deliveryDateLabel.topAnchor.constraint(equalTo: pickupDateLabel.bottomAnchor, constant: 20).isActive = true
        deliveryDateLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryDateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        deliveryDateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        pickupAddressLabel.topAnchor.constraint(equalTo: deliveryDateLabel.bottomAnchor, constant: 20).isActive = true
        pickupAddressLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        pickupAddressLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        pickupAddressLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        deliveryAddressLabel.topAnchor.constraint(equalTo: pickupAddressLabel.bottomAnchor, constant: 20).isActive = true
        deliveryAddressLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        deliveryAddressLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        deliveryAddressLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
        priceLabel.topAnchor.constraint(equalTo: deliveryAddressLabel.bottomAnchor, constant: 20).isActive = true
        priceLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        priceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12).isActive = true
        priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
        
    }

}
