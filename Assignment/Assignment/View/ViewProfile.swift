//
//  ViewProfile.swift
//  Assignment
//
//  Created by Vikneswaran on 28/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
import Firebase
class ViewProfile: UIViewController {

    var ref : DatabaseReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference(fromURL: "https://assignment-2b2b4.firebaseio.com/")
        navigationItem.title = "My Profile"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(handleUpdate))
        
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        
        setupView()
        retrieveClientsData()
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let emailLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 96, g: 125, b: 139)
        return view
    }()
    
    let addressTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Please enter your address here"
        textView.backgroundColor = UIColor.white
        textView.layer.cornerRadius = 15
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.font = .systemFont(ofSize: 16)
        textView.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let phoneTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Please enter your phone number"
        textView.backgroundColor = UIColor.white
        textView.layer.cornerRadius = 15
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.font = .systemFont(ofSize: 16)
        textView.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let icNumberTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Please enter your IC number"
        textView.backgroundColor = UIColor.white
        textView.layer.cornerRadius = 15
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.font = .systemFont(ofSize: 16)
        textView.backgroundColor = UIColor(r: 120, g: 144, b: 156)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let name: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = "Name:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let email: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = "Email:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let address: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = "Address:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let icNum: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = "Ic Num:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let phone: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.text = "Phone:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupView() {
        view.addSubview(container)
        container.addSubview(nameLabel)
        container.addSubview(emailLabel)
        container.addSubview(addressTextView)
        container.addSubview(phoneTextView)
        container.addSubview(icNumberTextView)
        container.addSubview(name)
        container.addSubview(email)
        container.addSubview(address)
        container.addSubview(phone)
        container.addSubview(icNum)
        
        //Mark:- Container
        container.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        container.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        container.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        
        name.topAnchor.constraint(equalTo: container.topAnchor, constant: 70).isActive = true
        name.heightAnchor.constraint(equalToConstant: 40).isActive = true
        name.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 5).isActive = true
        name.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        nameLabel.topAnchor.constraint(equalTo: container.topAnchor, constant: 70).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: name.trailingAnchor, constant: 2).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        email.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 30).isActive = true
        email.heightAnchor.constraint(equalToConstant: 40).isActive = true
        email.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 5).isActive = true
        email.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        emailLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 30).isActive = true
        emailLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        emailLabel.leadingAnchor.constraint(equalTo: email.trailingAnchor, constant: 2).isActive = true
        emailLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        address.topAnchor.constraint(equalTo: email.bottomAnchor, constant: 30).isActive = true
        address.heightAnchor.constraint(equalToConstant: 40).isActive = true
        address.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 5).isActive = true
        address.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        addressTextView.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 30).isActive = true
        addressTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        addressTextView.leadingAnchor.constraint(equalTo: address.trailingAnchor, constant: 2).isActive = true
        addressTextView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        phone.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 30).isActive = true
        phone.heightAnchor.constraint(equalToConstant: 40).isActive = true
        phone.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 5).isActive = true
        phone.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        phoneTextView.topAnchor.constraint(equalTo: addressTextView.bottomAnchor, constant: 30).isActive = true
        phoneTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        phoneTextView.leadingAnchor.constraint(equalTo: phone.trailingAnchor, constant: 2).isActive = true
        phoneTextView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
        icNum.topAnchor.constraint(equalTo: phone.bottomAnchor, constant: 30).isActive = true
        icNum.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icNum.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 5).isActive = true
        icNum.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        icNumberTextView.topAnchor.constraint(equalTo: phoneTextView.bottomAnchor, constant: 30).isActive = true
        icNumberTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        icNumberTextView.leadingAnchor.constraint(equalTo: icNum.trailingAnchor, constant: 2).isActive = true
        icNumberTextView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -12).isActive = true
        
    }

    func retrieveClientsData() {
        guard let userId = Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("Clients").child(userId).observe(.value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            self.nameLabel.text = (value?["name"] as? String)
            self.emailLabel.text = (value?["email"] as? String)
            self.addressTextView.text = value?["address"] as? String
            self.phoneTextView.text = value?["phone"] as? String
            self.icNumberTextView.text = value?["icNumber"] as? String
            
        }, withCancel: nil)
    }
    
    @objc func handleUpdate() {
        guard let userId = Auth.auth().currentUser?.uid else {return}
        let dataRef = self.ref?.child("Clients").child(userId)
        let values = ["address" : self.addressTextView.text as String,
                      "phone" : self.phoneTextView.text as String,
                      "icNumber" : self.icNumberTextView.text] as [String : Any]
        dataRef?.updateChildValues(values, withCompletionBlock: { (error, ref) in
            if error != nil {
                Alert.showPopup(title: "Error", message: (error?.localizedDescription)!, viewController: self)
            }
            Alert.showPopup(title: "Done", message: "Successfully Updated!", viewController: self)
        })
    }
}
