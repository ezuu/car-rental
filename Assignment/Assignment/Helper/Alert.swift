//
//  Alert.swift
//  Assignment
//
//  Created by Vikneswaran on 26/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    class func showPopup(title: String, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okButton = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(okButton)
        viewController.present(alert, animated: true, completion: nil)
    }
}
