//
//  Cars.swift
//  Assignment
//
//  Created by Vikneswaran on 27/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import Foundation
struct Cars {
    var carMake: String
    var carModel: String
    var isBooked: String
    var numberOfSeats: String
    var yearMake: String
    var cardId: String
}
