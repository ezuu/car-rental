//
//  Clients.swift
//  Assignment
//
//  Created by Vikneswaran on 28/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
struct Clients {
    var address : String
    var email : String
    var icNumber : String
    var name : String
    var phone : String
}
