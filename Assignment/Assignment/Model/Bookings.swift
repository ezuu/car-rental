//
//  Bookings.swift
//  Assignment
//
//  Created by Vikneswaran on 28/11/2017.
//  Copyright © 2017 Viknes. All rights reserved.
//

import UIKit
struct Bookings {
    var bookingDate : String
    var pickedCar : String
    var deliveryAddress : String
    var pickupAddress : String
    var pickupDate : String
    var deliveryDate : String
    var price : String
    var bookingId : String
    var bookedCar : String
}
